INSERT INTO products (id, name, description, price, is_available) VALUES (1, 'Ball', 'Just a ball', 2.50, true);
INSERT INTO products (id, name, description, price, is_available) VALUES (2, 'Wine', 'The cheap one', 4.00, false);

INSERT INTO reviews (id, product_id, content, rating) VALUES (1, 1, 'Cool', 5);
INSERT INTO reviews (id, product_id, content, rating) VALUES (2, 1, 'Lame', 2);
