package de.tkachov.mthesis.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.tkachov.mthesis.product.ProductEntity;
import de.tkachov.mthesis.product.ReviewEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@SpringBootTest
@ContextConfiguration(initializers = {ProductIntegrationTests.Initializer.class})
@AutoConfigureMockMvc
@Sql({"classpath:de/tkachov/mthesis/integration/schema.sql", "classpath:de/tkachov/mthesis/integration/data.sql"})
@Testcontainers
public class ProductIntegrationTests {

    @Autowired
    private MockMvc mockMvc;

    @Container
    private static final PostgreSQLContainer<?> databaseContainer = new PostgreSQLContainer<>("postgres:14.0");

    static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertyValues.of(
                    "spring.datasource.url=" + databaseContainer.getJdbcUrl(),
                    "spring.datasource.username=" + databaseContainer.getUsername(),
                    "spring.datasource.password=" + databaseContainer.getPassword()
            ).applyTo(configurableApplicationContext.getEnvironment());
        }
    }

    @BeforeAll
    public static void setUp() {

    }

    @Test
    public void test() throws Exception {

        HttpClient client = HttpClient.newHttpClient();
        HttpRequest postRequest = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:8080/products"))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofInputStream(() -> getClass().getResourceAsStream("de/tkachov/playground/integration/Product.json")))
                .build();


        HttpResponse<String> response = client.send(postRequest, HttpResponse.BodyHandlers.ofString());

        Assertions.assertEquals(response.statusCode(), 200);
    }

    @Test
    public void testNumberOfProducts() throws Exception {
        var result = mockMvc.perform(get("/products")).andReturn();
        var json = result.getResponse().getContentAsString();
        ObjectMapper objectMapper = new ObjectMapper();
        ProductEntity[] products = objectMapper.readValue(json, ProductEntity[].class);
        Assertions.assertEquals(2, products.length);
    }

    @Test
    public void testNumberOfRevies() throws Exception {
        var result = mockMvc.perform(get("/products/1/reviews")).andReturn();
        var json = result.getResponse().getContentAsString();
        ObjectMapper objectMapper = new ObjectMapper();
        ReviewEntity[] products = objectMapper.readValue(json, ReviewEntity[].class);
        Assertions.assertEquals(2, products.length);
    }
}
