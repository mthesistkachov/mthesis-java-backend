package de.tkachov.mthesis.integration;

import org.testcontainers.containers.PostgreSQLContainer;

public enum DatabaseContainer {
    INSTANCE;

    private final PostgreSQLContainer<?> container;

    DatabaseContainer() {
        container = new PostgreSQLContainer<>("postgres:14.0");
        container.start();

        System.setProperty("DB_URL", container.getJdbcUrl());
        System.setProperty("DB_USERNAME", container.getUsername());
        System.setProperty("DB_PASSWORD", container.getPassword());
    }
}
