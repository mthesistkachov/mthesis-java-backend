package de.tkachov.mthesis.product;

import de.tkachov.mthesis.product.ReviewEntity;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ReviewRepository extends Repository<ReviewEntity, Integer> {

    @Transactional(readOnly = true)
    ReviewEntity findById(Integer id);

    @Transactional(readOnly = true)
    List<ReviewEntity> findAllByProductId(Integer productId);

    void save(ReviewEntity review);
}
