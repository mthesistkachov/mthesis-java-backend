package de.tkachov.mthesis.product;

import de.tkachov.mthesis.user.UserEntity;

public class ReviewMapper {

    private ReviewMapper() {}

    public static ReviewEntity toEntity(
            ReviewCreationDTO dto,
            UserEntity user,
            int productId
    ) {
        return new ReviewEntity(
                dto.getTitle(),
                dto.getContent(),
                dto.getRating(),
                productId,
                user.getUserId()
        );
    }
}
