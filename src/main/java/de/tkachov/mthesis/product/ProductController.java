package de.tkachov.mthesis.product;

import de.tkachov.mthesis.user.UserEntity;
import de.tkachov.mthesis.user.UserService;
import de.tkachov.mthesis.util.HttpUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
public class ProductController {
    // TODO move logic into service

    private static final Logger LOG = LoggerFactory.getLogger(ProductController.class);

    private final ProductService productService;
    private final ProductRepository productsRepository;
    private final ReviewRepository reviewRepository;
    private final UserService userService;

    public ProductController(
            ProductService productService,
            ProductRepository products,
            ReviewRepository reviewRepository,
            UserService userService
    ) {
        this.productService = productService;
        this.productsRepository = products;
        this.reviewRepository = reviewRepository;
        this.userService = userService;
    }

    @GetMapping("/products")
    public ResponseEntity<Map<String, Object>> getAllProducts() {
        List<ProductEntity> products = productService.getAllProducts();
        LOG.info("Fetched {} products", products.size());
        return HttpUtil.dataResponse(products);
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<?> getProduct(@PathVariable("id") int id) {
        Optional<ProductEntity> product = productsRepository.findById(id);
        return product.map(HttpUtil::dataResponse).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/products/{id}/reviews")
    public ResponseEntity<?> getReviews(@PathVariable("id") int id) {
        List<ReviewEntity> reviewEntities = reviewRepository.findAllByProductId(id);

        List<ReviewDTO> reviews = new ArrayList<>();
        for (ReviewEntity entity : reviewEntities) {
            String userId = entity.getUserId();
            Optional<UserEntity> user = this.userService.getUserById(userId);
            if (user.isEmpty()) {
                LOG.warn("No user with id {} found", userId);
                continue;
            }
            UserEntity userEntity = user.get();
            ReviewDTO reviewDTO = new ReviewDTO(entity, userEntity);
            reviews.add(reviewDTO);
        }

        return HttpUtil.dataResponse(reviews);
    }

    @PostMapping("/products")
    public void createProduct(@RequestBody ProductEntity product) {
        productsRepository.save(product);
    }

    @PostMapping("/products/{id}/reviews")
    public void createReview(
            @RequestBody ReviewCreationDTO review,
            @PathVariable("id") int productId
    ) {
        LOG.debug("Creating review for productId={}", productId);
        UserEntity user = this.userService.getCurrentUser();

        ReviewEntity entity = ReviewMapper.toEntity(
                review,
                user,
                productId
        );

        this.reviewRepository.save(entity);
        LOG.debug("Created review for productId={}", productId);
    }

}
