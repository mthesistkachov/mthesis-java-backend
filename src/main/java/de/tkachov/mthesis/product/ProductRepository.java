package de.tkachov.mthesis.product;


import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface ProductRepository extends Repository<ProductEntity, Integer> {

    @Transactional(readOnly = true)
    Optional<ProductEntity> findById(Integer id);

    @Transactional(readOnly = true)
    List<ProductEntity> findAll();

    void save(ProductEntity product);
}
