package de.tkachov.mthesis.product;


import de.tkachov.mthesis.user.UserEntity;

public class ReviewDTO {

    private Integer id;
    private String title;
    private String content;
    private Integer rating;
    private Integer productId;
    private String reviewer;

    public ReviewDTO(ReviewEntity reviewEntity, UserEntity user) {
        this.title = reviewEntity.getTitle();
        this.id = reviewEntity.getId();
        this.content = reviewEntity.getContent();
        this.rating = reviewEntity.getRating();
        this.productId = reviewEntity.getProductId();
        this.reviewer = user.getFirstname() + " " + user.getLastname();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getReviewer() {
        return reviewer;
    }

    public void setReviewer(String reviewer) {
        this.reviewer = reviewer;
    }
}
