package de.tkachov.mthesis.comms;

import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

//@RestController
public class Controller {

    private final KafkaTemplate<String, String> publisher;


    public Controller(KafkaTemplate<String, String> publisher) {
        this.publisher = publisher;
    }


    @PostMapping("/comms/echo")
    public EchoDTO echo(@RequestBody EchoDTO dto) throws InterruptedException {
        String requestMessage = dto.getMessage();
        this.publisher.send("echo-request", requestMessage);
        this.publisher.flush();

        String responseMessage = EchoKafkaListener.INBOX.poll(10L, TimeUnit.SECONDS);
        EchoDTO responseDto = new EchoDTO();
        responseDto.setMessage(responseMessage);
        return responseDto;
    }
}
