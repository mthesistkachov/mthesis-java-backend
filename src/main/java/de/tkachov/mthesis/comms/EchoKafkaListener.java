package de.tkachov.mthesis.comms;

import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.KafkaListener;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

//@Configuration
public class EchoKafkaListener {

    static final BlockingQueue<String> INBOX = new LinkedBlockingQueue<>();

    @KafkaListener(topics = "echo-response", groupId = "mthesis")
    public void listen(String message) {
        INBOX.add(message);
    }
}
