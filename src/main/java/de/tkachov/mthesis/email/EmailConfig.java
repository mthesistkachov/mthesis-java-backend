package de.tkachov.mthesis.email;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

//@Configuration
public class EmailConfig {

    private static final String SMTP_SERVER = "";
    private static final int SMTP_PORT = 0;
    private static final String USER_NAME = "";
    private static final String PASS = "";

    static final String SENDER = "";
    static final String RECEIVER = "";

    @Bean
    public JavaMailSender javaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();

        mailSender.setHost(SMTP_SERVER);
        mailSender.setPort(SMTP_PORT);

        mailSender.setUsername(USER_NAME);
        mailSender.setPassword(PASS);

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");

        return mailSender;
    }
}
