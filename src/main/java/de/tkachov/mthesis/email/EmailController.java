package de.tkachov.mthesis.email;

import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.internet.MimeMessage;
import java.io.File;

//@RestController
public class EmailController {

    private final JavaMailSender mailSender;

    public EmailController(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    @PostMapping("/testemail")
    public void email() throws Exception {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);

        helper.setFrom(EmailConfig.SENDER);
        helper.setTo(EmailConfig.RECEIVER);

        helper.setSubject("Test email");
        helper.setText("This is just a test email");

        FileSystemResource file = new FileSystemResource(new File("email/attachment.txt"));
        helper.addAttachment("attachment.txt", file);

        mailSender.send(message);
    }
}
