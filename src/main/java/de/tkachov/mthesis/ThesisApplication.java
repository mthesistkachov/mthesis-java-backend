package de.tkachov.mthesis;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("de.tkachov")
@ConfigurationPropertiesScan("de.tkachov")
public class ThesisApplication {


	public static void main(String[] args) {
		SpringApplication.run(ThesisApplication.class, args);
	}

}
