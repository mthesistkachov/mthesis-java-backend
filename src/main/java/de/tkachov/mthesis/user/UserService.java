package de.tkachov.mthesis.user;

import org.keycloak.KeycloakPrincipal;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.authorization.client.AuthzClient;
import org.keycloak.authorization.client.Configuration;
import org.keycloak.representations.AccessToken;
import org.keycloak.representations.AccessTokenResponse;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class UserService {

    private final RealmResource realm;
    private final AuthzClient authClient;

    public UserService(KeycloakProperties properties) {
        Keycloak keycloak = KeycloakBuilder.builder()
                .serverUrl(properties.getUrl())
                .realm(properties.getRealm())
                .username(properties.getAdminName())
                .password(properties.getAdminPassword())
                .clientId(properties.getClientName())
                .clientSecret(properties.getClientPassword())
                .grantType(OAuth2Constants.PASSWORD)
                .build();

        this.realm = keycloak.realm("dev");

        Map<String, Object> clientCredentials = Map.of(
                "secret", properties.getClientPassword(),
                "grant_type", "password"
        );

        Configuration authClientConfiguration = new Configuration(
                properties.getUrl(),
                properties.getRealm(),
                properties.getClientName(),
                clientCredentials,
                null
        );

        this.authClient = AuthzClient.create(authClientConfiguration);
    }

    private CredentialRepresentation convertPasswordToCredential(String password) {
        CredentialRepresentation credentials = new CredentialRepresentation();
        credentials.setTemporary(false);
        credentials.setType(CredentialRepresentation.PASSWORD);
        credentials.setValue(password);
        return credentials;
    }

    private UserEntity mapUserRepresentation(UserRepresentation representation) {
        UserEntity user = new UserEntity();
        user.setUserId(representation.getId());
        user.setUsername(representation.getUsername());
        user.setFirstname(representation.getFirstName());
        user.setLastname(representation.getLastName());
        return user;
    }

    public boolean isUsernameAvailable(String username) {
        List<UserRepresentation> result = realm.users().search(username, true);
        return result.size() == 0;
    }

    public ResponseEntity<?> loginUser(String username, String password) {
        try {
            AccessTokenResponse response = authClient.obtainAccessToken(username, password);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    public int createUser(UserEntity userEntity) {
        CredentialRepresentation credentials = convertPasswordToCredential(userEntity.getPassword());

        UserRepresentation user = new UserRepresentation();
        user.setUsername(userEntity.getUsername());
        user.setFirstName(userEntity.getFirstname());
        user.setLastName(userEntity.getLastname());
        user.setCredentials(Collections.singletonList(credentials));
        user.setEnabled(true);

        return realm.users().create(user).getStatus();
    }

    public UserEntity getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object principal = authentication.getPrincipal();
        KeycloakPrincipal keycloakPrincipal = (KeycloakPrincipal) principal;
        AccessToken token = keycloakPrincipal.getKeycloakSecurityContext().getToken();

        UserEntity user = new UserEntity();
        user.setUserId(token.getSubject());
        user.setUsername(token.getPreferredUsername());
        user.setFirstname(token.getGivenName());
        user.setLastname(token.getFamilyName());
        return user;
    }

    public Optional<UserEntity> getUserById(String id) {
        String query = "id:" + id;
        List<UserRepresentation> users = realm.users().search(query, 0, 1, true);

        if (users.isEmpty()) {
            return Optional.empty();
        }

        UserRepresentation userRepresentation = users.get(0);
        UserEntity userEntity = this.mapUserRepresentation(userRepresentation);

        return Optional.of(userEntity);
    }
}
