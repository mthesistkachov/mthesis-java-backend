package de.tkachov.mthesis.user;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;


@ConstructorBinding
@ConfigurationProperties("mthesis.keycloak")
public class KeycloakProperties {

    private final String url;
    private final String realm;
    private final String clientName;
    private final String clientPassword;
    private final String adminName;
    private final String adminPassword;

    public KeycloakProperties(
            String url,
            String realm,
            String clientName,
            String clientPassword,
            String adminName,
            String adminPassword
    ) {
        this.url = url;
        this.realm = realm;
        this.clientName = clientName;
        this.clientPassword = clientPassword;
        this.adminName = adminName;
        this.adminPassword = adminPassword;
    }

    public String getUrl() {
        return url;
    }

    public String getRealm() {
        return realm;
    }

    public String getClientName() {
        return clientName;
    }

    public String getClientPassword() {
        return clientPassword;
    }

    public String getAdminName() {
        return adminName;
    }

    public String getAdminPassword() {
        return adminPassword;
    }
}
