package de.tkachov.mthesis.user;

import de.tkachov.mthesis.util.HttpUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping(path = "/auth")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PutMapping("/usernameavailability")
    public ResponseEntity<Map<String, Boolean>> isUsernameAvailable(@RequestBody UserEntity userEntity) {
        boolean available = this.userService.isUsernameAvailable(userEntity.getUsername());
        return ResponseEntity.ok(
                Map.of("available", available)
        );
    }

    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@RequestBody UserEntity userEntity) {
        int status = this.userService.createUser(userEntity);

        if (HttpUtil.isSuccessful(status)) {
            return this.userService.loginUser(userEntity.getUsername(), userEntity.getPassword());
        }

        String message =  "Failed to create the user";
        return HttpUtil.simpleResponse(status, message);
    }

    @PostMapping("/login")
    public ResponseEntity<?> loginUser(@RequestBody UserEntity userEntity) {
        String username = userEntity.getUsername();
        String password = userEntity.getPassword();

        return userService.loginUser(username, password);
    }

    @GetMapping("/me")
    public UserEntity userInformation(@RequestHeader("Authorization") String authHeader) {
        return this.userService.getCurrentUser();
    }

}
