package de.tkachov.mthesis.templating;

import de.tkachov.mthesis.product.ProductEntity;
import de.tkachov.mthesis.product.ProductService;
import de.tkachov.mthesis.user.UserEntity;
import de.tkachov.mthesis.user.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class TemplatingController {

    private final UserService userService;
    private final ProductService productService;
    private final FreemarkerEngine templateEngine;

    public TemplatingController(UserService userService, ProductService productService, FreemarkerEngine templateEngine) {
        this.userService = userService;
        this.productService = productService;
        this.templateEngine = templateEngine;
    }

    @GetMapping("/templating")
    public Map<String, String> buildFromTemplate() throws Exception {
        UserEntity user = this.userService.getCurrentUser();
        List<ProductEntity> products = this.productService.getAllProducts();

        String fullname = user.getFirstname() + " " + user.getLastname();
        long n = System.currentTimeMillis() % 60;
        boolean even = n % 2 == 0;

        Map<String, Object> model = Map.of(
                "fullname", fullname,
                "even", even,
                "products", products
        );

        String out = this.templateEngine.process("demo.ftl", model);
        System.out.println(out);

        return Map.of("text", out);
    }
}
