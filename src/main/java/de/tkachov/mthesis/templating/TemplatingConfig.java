package de.tkachov.mthesis.templating;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TemplatingConfig {

    @Bean
    public freemarker.template.Configuration freemarkerConfig() {
        freemarker.template.Configuration config = new freemarker.template.Configuration(freemarker.template.Configuration.VERSION_2_3_31);

        config.setClassForTemplateLoading(this.getClass(), "/templates/");
        config.setDefaultEncoding("UTF-8");

        return config;
    }
}
