package de.tkachov.mthesis.templating;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.StringWriter;

@Component
public class FreemarkerEngine {

    private final Configuration config;

    public FreemarkerEngine(@Qualifier("freemarkerConfig") Configuration config) {
        this.config = config;
    }

    public String process(String templateName, Object model) throws IOException, TemplateException {
        Template template = this.config.getTemplate(templateName);

        StringWriter writer = new StringWriter();
        template.process(model, writer);

        return writer.toString();
    }
}
