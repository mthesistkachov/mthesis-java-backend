package de.tkachov.mthesis.util;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Map;

public class HttpUtil {

    private HttpUtil() {
    }

    public static boolean isSuccessful(int code) {
        return code >= 200 && code < 300;
    }

    public static ResponseEntity<Map<String, String>> simpleResponse(int code, String message) {
        String key = isSuccessful(code) ? "msg" : "err";
        return new ResponseEntity<>(
                Map.of(key, message),
                HttpStatus.valueOf(code)
        );
    }

    public static ResponseEntity<Map<String, Object>> dataResponse(Object data) {
        return new ResponseEntity<Map<String, Object>>(
                Map.of("data", data),
                HttpStatus.OK
        );
    }
}
