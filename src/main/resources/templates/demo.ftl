Hello ${fullname}

<#if even>
Your secret random number is even!
<#else>
Your secret random number is odd...
</#if>

You may be interested in the following products:
<#list products as p>
    * ${p.name} at ${p.price} $
</#list>
