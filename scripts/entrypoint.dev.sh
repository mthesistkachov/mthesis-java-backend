#!/bin/bash

set -e

# Build once in foreground to make sure everything is up to date
sh gradlew build -x test || true

# Observe file changes in the background
# If something changes the application is recompiled
sh gradlew -t build -x test &

# Run the application
# Spring dev-tools are enabled so the app restarts when the continuous gradle build rebuilds
while true; do
  sh gradlew run
  echo "Restarting in 5 seconds..."
  sleep 5
done
